
var loc = window.location.href;
var dir = loc.substring(0, loc.lastIndexOf('/'));
var baseAssetsPath=dir+"/";
var campaignImagesPath=baseAssetsPath+"assets/imgs/campaign/"
var fname,jtitle,officeLocation,contactOffice,contactMobile,contactVOIP,campaignSelectedImagePath;

//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
    var btn_id=this.id;
    var err="";     

    switch(btn_id){
        case "btn_submit_personal_info":
            fname=$("#input_fname").val();
            jtitle=$("#input_jtitle").val();
            officeLocation=$("#select_location").val();

            if($.isEmptyObject(fname)){
                err="Full Name";
                $("#input_fname").css('border', '1px solid red'); 
            }else{
                $("#input_fname").css('border', '1px solid #ccc'); 
            } 

            if($.isEmptyObject(officeLocation)){
                err+="<br>Location";
                $("#select_location").css('border', '1px solid red'); 
            }else{
                $("#select_location").css('border', '1px solid #ccc'); 
            }

            break;
        case "btn_submit_contact_info":
            contactOffice=$("#input_office").val();
            contactMobile=$("#input_mobile").val();
            contactVOIP=$("#input_voip").val();

            if($.isEmptyObject(contactOffice)){
                err="Office Contact";
                $("#input_office").css('border', '1px solid red'); 
            }
            break;
        case "btn_skip_campaign":  
            var generated_sign_container=$("#generated_signature_container");            generateSign(generated_sign_container,fname,jtitle,officeLocation,contactOffice,contactMobile,contactVOIP,"");

            $("#fieldset_sign_output").css('height','');
            break;

        case "btn_submit_campaign":
            var generated_sign_container=$("#generated_signature_container");            generateSign(generated_sign_container,fname,jtitle,officeLocation,contactOffice,contactMobile,contactVOIP,campaignSelectedImagePath);
            break;        
        default:
            break;
    }

    if(!$.isEmptyObject(err)){
        //alert(err);
        swal({
            title: "Missing Form Values",
            html: err,
            type: "error",
        });
    }else{

        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale('+scale+')',
                    'position': 'absolute'
                });
                next_fs.css({'left': left, 'opacity': opacity});
            }, 
            duration: 800, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });   
    }
});

$(".previous").click(function(){
    if(animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show(); 
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
        }, 
        duration: 800, 
        complete: function(){
            current_fs.hide();
            animating = false;
        }, 
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".submit").click(function(){
    return false;
})

//Cross-browser function to select content
function selectText(element) {
    var doc = document;
    if (doc.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}

function copySign(){
    var targetDiv=$("#generated_signature_container");
    //Make the container Div contenteditable
    targetDiv.attr("contenteditable", true);
    //Select the image
    selectText(targetDiv.get(0));
    //Execute copy Command
    //Note: This will ONLY work directly inside a click listenner
    document.execCommand('copy');
    //Unselect the content
    window.getSelection().removeAllRanges();
    //Make the container Div uneditable again
    targetDiv.removeAttr("contenteditable");
    //Success!!    
}


function campaignSelection(campaignSelect){
    var campaignImgContainer=$("#img_campaign_image");

    switch (campaignSelect.selectedOptions[0].value) {
        case 'sap':     
            campaignSelectedImagePath=campaignImagesPath+"sap.jpg";
            campaignImgContainer.attr("src",campaignImagesPath+"sap.jpg");         
            break;
        case 'digital':     
            campaignSelectedImagePath=campaignImagesPath+"digital.jpg"
            campaignImgContainer.attr("src",campaignImagesPath+"digital.jpg");    
            break;
        case 'testing':  
            campaignSelectedImagePath=campaignImagesPath+"testing.jpg"
            campaignImgContainer.attr("src",campaignImagesPath+"testing.jpg");    
            break;
        case 'integration':  
            campaignSelectedImagePath=campaignImagesPath+"sap.jpg";
            campaignImgContainer.attr("src",campaignImagesPath+"sap.jpg");    
            break;
        case 'bfs':
            campaignSelectedImagePath=campaignImagesPath+"digital.jpg";
            campaignImgContainer.attr("src",campaignImagesPath+"digital.jpg");    
            break;
        default:
            break;

    }

    $("#fieldset_sign_output").css('height','450px');
}

function locationSelectEventListener(locationSelect) {   
    var cleavePhoneVOIP = new Cleave('#input_voip', {
        numericOnly: true,
        blocks: [6]
    });

    switch (locationSelect.selectedOptions[0].value) {
        case 'Mumbai, India':           
            $("#input_office").val("+91 22 4504 7600");

            $("#input_mobile").attr("placeholder", "+91");

            var cleavePhoneOfc = new Cleave('#input_office', {
                phone: true,
                phoneRegionCode: 'IN'
            });
            var cleavePhoneMobile = new Cleave('#input_mobile', {
                phone: true,
                phoneRegionCode: 'IN'
            });

            break;
        case 'Bengaluru, India':
            $("#input_office").val("+91 80 4651 9300");  

            $("#input_mobile").attr("placeholder", "+91");

            var cleavePhoneOfc = new Cleave('#input_office', {
                phone: true,
                phoneRegionCode: 'IN'
            });
            var cleavePhoneMobile = new Cleave('#input_mobile', {
                phone: true,
                phoneRegionCode: 'IN'
            });

            break;
        case 'Chicago, IL USA':
            $("#input_office").val("+1 312 219 6500");   

            $("#input_mobile").attr("placeholder", "+1");

            var cleavePhoneOfc = new Cleave('#input_office', {
                phone: true,
                phoneRegionCode: 'US'
            });
            var cleavePhoneMobile = new Cleave('#input_mobile', {
                phone: true,
                phoneRegionCode: 'US'
            });

            break;
        case 'London, UK':
            $("#input_office").val("+44 207 153 1800");   

            $("#input_mobile").attr("placeholder", "+44");

            var cleavePhoneOfc = new Cleave('#input_office', {
                phone: true,
                phoneRegionCode: 'GB'
            });
            var cleavePhoneMobile = new Cleave('#input_mobile', {
                phone: true,
                phoneRegionCode: 'GB'
            });

            break;
        default:
            break;
    }

    $("#select_location").css('border', '1px solid #ccc'); 
}

function generateSign(divName,fullName,jobTitle,officeLocation,contactOffice,contactMobile,contactVOIP,campaignImage){
    var div_output_container=divName;
    div_output_container.html("");
    div_output_container.css("background","white");
    /*div_output_container.css("height","240px");
    div_output_container.css("width","630px");*/
    div_output_container.css("width","100%");
    div_output_container.css("max-width","630px");
    div_output_container.css("max-height","240px");

    if(officeLocation!=null && officeLocation=='Select')
        officeLocation="";

    if(!contactOffice=="")
        contactOffice='T:'+contactOffice;

    if(!contactMobile=="")
        contactMobile=' | M:'+contactMobile; 

    if(!contactVOIP=="")
        contactVOIP=' | VOIP:'+contactVOIP;

    if(campaignImage==undefined || campaignImage==null)
        campaignImage="";

    div_output_container.html('<div style="max-width:100%;font-family: Verdana;"><table> <tbody> <tr> <td> <img class="output_img" src="'+baseAssetsPath+'assets/imgs/quinnox_chevron.png"> </td> <td class="table_td_chevron_left_padding"> <table> <tbody> <tr> <td> <span class="sign_font_header" style="color: rgb(0,92,171);"><big><strong>'+fullName+'</strong></big></span> </td> </tr> <tr> <td> <span class="sign_font">'+jobTitle+'</span> </td></tr> <tr> </tr> <tr></tr> <tr> <tr> <td><span class="sign_font"> '+officeLocation+' </span></td> </tr> <tr> <td> <span class="sign_font">'+contactOffice+''+contactMobile+''+contactVOIP+'</span> </td> </tr> <tr> <td> <div style="margin-top: 5px;padding-top: 5px;border-top-color: lightgray;border-top-style: dotted;"> <a href="http://www.quinnox.com" target="_blank"><img class="output_img_footer_qnx" src="'+baseAssetsPath+'assets/imgs/qxn.png"></a></img> <a href="http://www.facebook.com" target="_blank"><img class="output_img_footer"  src="'+baseAssetsPath+'assets/imgs/fb.png"></a> <a href="http://www.twitter.com" target="_blank"><img class="output_img_footer"   src="'+baseAssetsPath+'assets/imgs/twitter.png"></a> <a href="http://www.linkedin.com" target="_blank"><img class="output_img_footer" src="'+baseAssetsPath+'assets/imgs/linkedIn.png"></a> </div> <td></tr></tbody> </table> </td> </tr></tbody> </table><img class="campaign_img" src='+campaignImage+'></div>');
}

function downloadInnerHtml(filename, elId, mimeType) {
    var elHtml = '<meta name="viewport" content="width=device-width, initial-scale=1.0"> <style>body{ max-width:630px !important; max-height:240px !important; min-width: 545px !important; font-family: "Verdana", arial, verdana } .sign_font { font-size: 0.7em; } .output_img { width: 100%; min-width: 95px !important; max-width: 155px !important; } .table_td_chevron_left_padding{ padding-left:10px; } .output_img_footer{ max-width:100%; max-width: 30px !important; min-width: 23px !important; } .output_img_footer_qnx{ max-width:100%; max-width: 104px !important; } .campaign_img{max-width:100%;max-width: 630px !important;    } @media (max-width: 767px) { .output_img { width: 80%; } .sign_font_header { font-size: 0.7em; } .sign_font { font-size: 0.6em; } .table_td_chevron_left_padding{ padding-left:5px; } .output_img_footer{ width:7%; } .output_img_footer_qnx{ width:25%; } .campaign_img{width: 80%;}}</style>'+document.getElementById(elId).innerHTML;

    if (navigator.msSaveBlob) { // IE 10+ 
        navigator.msSaveBlob(new Blob([elHtml], { type: mimeType + ';charset=utf-8;' }), filename);
    } else {
        var link = document.createElement('a');
        mimeType = mimeType || 'text/plain';

        link.setAttribute('download', filename);
        link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(elHtml));  

        /* var metaTag=document.createElement("meta");
        metaTag.name = "viewport";
        metaTag.content = "width=device-width, initial-scale=1.0, maximum-scale=1.0";        
        document.head.appendChild(metaTag);*/

        document.body.append(link);
        link.click(); 
        document.body.removeChild(link);        
    }


    setTimeout(function() { 
        swal({
            title: "Signature Save Path Info",
            html:"You have sucessfully downloaded your signature.<br>Now navigate to<br><strong><u>C:\\Users\\%username%\\AppData\\Roaming\\Microsoft\\Signatures</u></strong><br>and paste the .html file you downloaded.<br>On completion, you should see the signature in your MS Outlook signature options." ,
            type: 'success'
        });
    }, 2000);


}
